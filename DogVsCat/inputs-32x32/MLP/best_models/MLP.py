import glob, os
import numpy as np

import keras
from keras.losses import *
from keras.activations import *
from keras.metrics import *
from keras.layers import *
from keras.models import *
from keras.callbacks import *
from keras.optimizers import *
from keras.preprocessing.image import array_to_img, img_to_array, load_img


#load data
dataset_dir_path = '/home/dim/datasets/dogs_cats/train_32x32/'

def load_dataset(dataset_dir_path, dataset_format, validation_percent):

    inputs = []
    labels = []

    #Read directory
    for img_path in glob.glob(os.path.join(dataset_dir_path, "*." + dataset_format)):

        base_name = os.path.basename(img_path)
        file, ext = os.path.splitext(base_name)

        #get image's label from file namet
        label = os.path.splitext(file)[0]

        #get image to tensor
        img = load_img(img_path)
        x = img_to_array(img)  #(32, 32, 3)
        x = x.flatten()



        inputs.append(x)
        if label == 'cat':
            labels.append([0, 1])
        elif label == 'dog':
            labels.append([1, 0])
        else:
            labels.append([0, 0])

    x_train_len = ((len(inputs) * validation_percent) // 100)


    x_test = np.array(inputs[0: x_train_len])
    y_test = np.array(labels[0: x_train_len])

    x_train = np.array(inputs[x_train_len:])
    y_train = np.array(labels[x_train_len:])

    x_train = x_train / 255
    x_test = x_test / 255

    nb_flatten_inputs = len(x_train[0].flatten())

    return (x_train, y_train),(x_test, y_test), nb_flatten_inputs

(x_train, y_train),(x_test, y_test), nb_flatten_inputs = load_dataset(dataset_dir_path, "png", 25)
print("Number of training example : " + str(len(x_train)))
print("Number of (training)labels example : " + str(len(y_train)))

print("Number of validation example : " + str(len(x_test)))
print("Number of (validation)labels example : " + str(len(y_test)))



############
#********************************
#   MLP 120 120
#********************************
############
# DONE
def epoch_change():
    # Hyper parameters
    learning_rate = 0.001
    momentum = 0.05

    func_activation = 'sigmoid'
    batch_size = 32
    hidden1 = 120
    hidden2 = 120

    # loop to change epochs for base
    for epochs in range(200, 1200, 200):

        # Setting callback report
        model_name = "Base_MPL_" + str(hidden1) + '-' \
                     + str(hidden2) + '-' + func_activation + '_SGD(lr' + str(learning_rate) + '-' \
                    + 'momentum-' + str(momentum) + '-batch_size-' + str(batch_size) + '-epochs-' + str(epochs)

        logdir = "/home/dim/models-logs/dog_cat/inputs-32x32/MLP/arch_change/base/"
        tensorboard_callback = TensorBoard(logdir + model_name, write_graph=True)

        checkpoint_name = '/home/dim/Documents/5-IBD/DeepLearning/project/DogVsCat/inputs-32x32/MLP/best_models/arch_change/base/best-' + model_name + '.h5'
        checkpoint = ModelCheckpoint(checkpoint_name, monitor='val_loss', verbose=0, save_best_only=True, mode='auto')


        #model build
        model = Sequential()


        model.add(Dense(hidden1, activation=func_activation, input_shape=(nb_flatten_inputs,)))
        model.add(Dense(hidden2, activation=func_activation))

        model.add(Dense(2, activation=sigmoid))

        model.compile(loss=categorical_crossentropy, optimizer=sgd(lr=learning_rate, momentum=momentum), metrics=[categorical_accuracy])

        model.summary()

        model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_test, y_test), callbacks=[tensorboard_callback, checkpoint])

        # Evaluate the model
        scores = model.evaluate(x_test, y_test, verbose = 0)
        print('Final accuracy:', str(scores[1] * 100), '%')



####################################################################
#hidden layer augmentation , network depth
def arch_aug_network_depth():
    # Hyper parameters
    learning_rate = 0.001
    momentum = 0.05
    epochs = 1200
    func_activation = 'sigmoid'
    batch_size = 32

    hidden_neurons = 120

    # loop to change number of hidden layer
    for nb_hidden_layer in range(3, 7, 1):
        # Setting callback report
        model_name = "Base_MPL_" + str(nb_hidden_layer) + 'hidden-' + str(hidden_neurons) + '-' + func_activation + '_SGD(lr' + str(learning_rate) + '-' \
                     + 'momentum-' + str(momentum) + '-batch_size-' + str(batch_size) + '-epochs-' + str(epochs)

        logdir = "/home/dim/models-logs/dog_cat/inputs-32x32/MLP/arch_change/hidden_depth/"
        tensorboard_callback = TensorBoard(logdir + model_name, write_graph=True)

        checkpoint_name = '/home/dim/Documents/5-IBD/DeepLearning/project/DogVsCat/inputs-32x32/MLP/best_models/arch_change/hidden_depth/best-' + model_name + '.h5'
        checkpoint = ModelCheckpoint(checkpoint_name, monitor='val_loss', verbose=0, save_best_only=True, mode='auto')

        # model build
        model = Sequential()

        model.add(Dense(hidden_neurons, activation=func_activation, input_shape=(nb_flatten_inputs,)))

        for i in range(0, nb_hidden_layer):
            model.add(Dense(hidden_neurons, activation=func_activation))

        model.add(Dense(2, activation=sigmoid))

        model.compile(loss=categorical_crossentropy, optimizer=sgd(lr=learning_rate, momentum=momentum),
                      metrics=[categorical_accuracy])

        model.summary()

        model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_test, y_test),
                  callbacks=[tensorboard_callback, checkpoint])

        # Evaluate the model
        scores = model.evaluate(x_test, y_test, verbose=0)
        print('Final accuracy:', str(scores[1] * 100), '%')


def linear_reg():

    learning_rate = 0.001
    epochs = 1500
    func_activation = 'sigmoid'
    batch_size = 164

    model_name = 'LinearReg_mse_sgd(lr' + str(learning_rate) + ')_epochs-' + str(epochs)
    logdir = "/home/dim/Documents/5-IBD/DeepLearning/project/DogVsCat/models-logs/LinearReg/base/"
    tensorboard_callback = TensorBoard(logdir + model_name, write_graph=True)

    checkpoint_name = '/home/dim/Documents/5-IBD/DeepLearning/project/DogVsCat/inputs-32x32/MLP/best_models/linearReg/best-' + model_name + '.h5'
    checkpoint = ModelCheckpoint(checkpoint_name, monitor='val_loss', verbose=0, save_best_only=True, mode='auto')

    model = Sequential()
    model.add(Dense(2, activation=func_activation, input_shape=(nb_flatten_inputs,)))

    model.compile(loss=mse, optimizer=sgd(lr=learning_rate), metrics=[categorical_accuracy])
    model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_test, y_test),
                  callbacks=[tensorboard_callback, checkpoint])

    # Evaluate the model
    scores = model.evaluate(x_test, y_test, verbose=0)
    print('Final accuracy:', str(scores[1] * 100), '%')

#arch_aug_network_depth()

linear_reg()