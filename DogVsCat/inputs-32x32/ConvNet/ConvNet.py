import glob, os
import numpy as np

import keras
from keras.losses import *
from keras.activations import *
from keras.metrics import *
from keras.layers import *
from keras.models import *
from keras.callbacks import *
from keras.optimizers import *
from keras.preprocessing.image import array_to_img, img_to_array, load_img


#load data
dataset_dir_path = '/home/dim/datasets/dogs_cats/train_32x32/'

def load_dataset(dataset_dir_path, dataset_format, validation_percent):

    inputs = []
    labels = []

    #Read directory
    for img_path in glob.glob(os.path.join(dataset_dir_path, "*." + dataset_format)):

        base_name = os.path.basename(img_path)
        file, ext = os.path.splitext(base_name)

        #get image's label from file namet
        label = os.path.splitext(file)[0]

        #get image to tensor
        img = load_img(img_path)
        x = img_to_array(img)  #(32, 32, 3)




        inputs.append(x)
        if label == 'cat':
            labels.append([0, 1])
        elif label == 'dog':
            labels.append([1, 0])
        else:
            labels.append([0, 0])

    x_train_len = ((len(inputs) * validation_percent) // 100)


    x_test = np.array(inputs[0: x_train_len])
    y_test = np.array(labels[0: x_train_len])

    x_train = np.array(inputs[x_train_len:])
    y_train = np.array(labels[x_train_len:])

    x_train = x_train / 255
    x_test = x_test / 255



    return (x_train, y_train),(x_test, y_test)

(x_train, y_train),(x_test, y_test) = load_dataset(dataset_dir_path, "png", 25)
print("Number of training example : " + str(len(x_train)))
print("Number of (training)labels example : " + str(len(y_train)))

print("Number of validation example : " + str(len(x_test)))
print("Number of (validation)labels example : " + str(len(y_test)))



############
#********************************
#   MLP 120 120
#********************************
############
# DONE
    # Hyper parameters
learning_rate = 0.001
momentum = 0.05

func_activation = 'relu'
batch_size = 32
conv1 = 16 * 4
conv2 = 32 * 4
conv3 = 64 * 4

epochs = 1200

hidden1 = 120
hidden2 = 120

kernel_conv = (3, 3)
pool_size = (2, 2)

    # loop to change epochs for base
    #for epochs in range(200, 1200, 200):

        # Setting callback report
model_name = "ConvNet_" + str(conv1) + '-pool-2-'  \
                     + str(conv2) + 'pool-2-' + str(conv3) + '-pool-2-krnel-(3*3)' '-ac' + func_activation + '-Fully-' + str(hidden1) + '-2_SGD(lr' + str(learning_rate) + '-' \
                    + 'momentum-' + str(momentum) + '-batch_size-' + str(batch_size) + '-epochs-' + str(epochs)
logdir = "/home/dim/models-logs/dog_cat/inputs-32x32/ConvNet/filter_change/"
tensorboard_callback = TensorBoard(logdir + model_name, write_graph=True)
checkpoint_name = '/home/dim/Documents/5-IBD/DeepLearning/project/DogVsCat/inputs-32x32/ConvNet/best_models/filter_change/best-' + model_name + '.h5'
checkpoint = ModelCheckpoint(checkpoint_name, monitor='val_loss', verbose=0, save_best_only=True, mode='auto')

        #model build

model = Sequential()

model.add(Conv2D(conv1,kernel_size=kernel_conv,  activation=func_activation, input_shape=(32, 32, 3)))
model.add(Dropout(0.25))
model.add(MaxPool2D(pool_size=pool_size))

model.add(Conv2D(conv2,kernel_size=kernel_conv, activation=func_activation))
model.add(Dropout(0.25))
model.add(MaxPool2D(pool_size=pool_size))

model.add(Conv2D(conv3, kernel_size=kernel_conv, activation=func_activation))
model.add(Dropout(0.25))
model.add(MaxPool2D(pool_size=pool_size))

model.add(Flatten())

model.add(Dense(hidden1, activation="relu"))
model.add(Dense(2, activation=sigmoid))
model.compile(loss=categorical_crossentropy, optimizer=sgd(lr=learning_rate, momentum=momentum), metrics=[categorical_accuracy])

model.summary()

model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_test, y_test), callbacks=[tensorboard_callback, checkpoint])

        # Evaluate the model
scores = model.evaluate(x_test, y_test, verbose = 0)
print('Final accuracy:', str(scores[1] * 100), '%')

