import tensorflow

# get MNIST DATASET
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data', one_hot=True) # one hot encode labels

import tensorflow as tf

X = tf.placeholder(tf.float32, shape=[None, 784], name='InputData')
Y = tf.placeholder(tf.float32, shape=[None, 10], name='Labels')

#set weights and bias and initialize it
weights = tf.Variable(tf.zeros([784, 10]), name='Weights')
bias = tf.Variable(tf.zeros([10]), name='Bias')

def get_weigths_variable(layer_name):
    with tf.variable_scope(layer_name, reuse=True):
        variable = tf.get_variable('weights')

    return variable

#Construct model and encapsulate ops

y = tf.matmul(X, weights) + bias
pred = tf.nn.sigmoid(y, name='sigmoid')


#Minimize error using cross entropy
#loss = tf.reduce_mean(-tf.reduce_sum(Y*tf.log(pred), reduction_indices=1))
#Minimize error using mean square error
error = pred - Y
cost = tf.reduce_mean(tf.square(error), name="mse")


#Accuracy
accuracy = tf.equal(tf.argmax(pred, 1), tf.argmax(Y, 1))
accuracy = tf.reduce_mean(tf.cast(accuracy, tf.float32))

#test_loss
train_loss_summary = tf.summary.scalar('loss', cost)
# sumary to monitor accuracy on train
train_acc_summary = tf.summary.scalar('acc', accuracy)

# validation_loss
val_loss_summary = tf.summary.scalar('val_loss', cost)
# sumary to monitor accuracy on test
val_acc_summary = tf.summary.scalar('val_acc', accuracy)


train_summary = tf.summary.merge([train_loss_summary, train_acc_summary])
val_summary = tf.summary.merge([val_loss_summary, val_acc_summary])

import os
from models.logs import LOG_DIR
from models.model import MODEL_DIR
# LOOP RULES!!!! :-D
learning_rate = 0.0001
while learning_rate <= 0.1:

    training_epochs = 200
    batch_size = 100
    log_path = os.path.join(LOG_DIR, 'LinearRegression','lr-' + str(learning_rate))
    best_model_path = os.path.join(MODEL_DIR, 'LinearRegression', 'best-lr' + str(learning_rate) + '.ckpt')

    summary_writer = tf.summary.FileWriter(log_path, graph=tf.get_default_graph())

    with tf.name_scope('SGD'):
        #Gradient descent
        optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

    #Initialize Variables weight and bias
    init = tf.global_variables_initializer()

    # save variables
    saver = tf.train.Saver()


    #Start training
    with tf.Session() as sess:

        sess.run(init)


        min_loss = 20
        #Training
        for epoch in range(training_epochs):

            print("Epoch:", '%04d' % (epoch + 1))
            avg_cost = 0.
            train_acc = 0.
            total_batch = int(mnist.train.num_examples/batch_size)

            for i in range(total_batch - 1):

                batch_xs, batch_ys = mnist.train.next_batch(batch_size)
                _, loss, acc = sess.run([optimizer, cost, accuracy], feed_dict={X: batch_xs, Y: batch_ys})

                # Compute average loss and average accuray (on test)
                avg_cost += loss / total_batch
                train_acc += acc / total_batch

            #compute last batch
            batch_xs, batch_ys = mnist.train.next_batch(batch_size)
            _, loss, acc, t_summary = sess.run([optimizer, cost, accuracy, train_summary], feed_dict={X: batch_xs, Y: batch_ys})
            # Compute average loss and average accuray (on test)
            avg_cost += loss / total_batch
            train_acc += acc / total_batch

            # Write logs at every iteration
            summary_writer.add_summary(t_summary, epoch + 1)

            # compute loss and accuracy (on validation)
            val_cost, val_acc, v_summary = sess.run([cost, accuracy, val_summary], {X: mnist.train.images, Y: mnist.train.labels})
            summary_writer.add_summary(v_summary, epoch + 1)

            val_acc_ = accuracy.eval(feed_dict={X: mnist.test.images, Y: mnist.test.labels})
            print(val_acc_)
            print("loss=", "{:.9f}".format(avg_cost),"acc=", "{:0.9f}".format(train_acc), "val_loss", "{:.9f}".format(val_cost), "val_acc=", "{:0.9f}".format(val_acc))

            if val_cost < min_loss:
                min_loss = val_cost
                save_path = saver.save(sess, best_model_path)
                print("Best model saved in file: " + save_path)

        sess.close()

    learning_rate *= 10




